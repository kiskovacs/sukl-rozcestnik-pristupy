"use strict"

function Main() {
    this.container = document.getElementById('container');
    this.scrollerImg = document.getElementById('scrollPrompter');
    this.table1 = document.getElementsByClassName('btn-1-pos')[0];
    /* min width for desktop */
    this.startMR = 767;
    this.resizeEnd = true;
}

Main.prototype = {
    scrolling: function(self, e) {
        if(window.pageYOffset > 0) {
            self.scrollerImg.style.opacity = 0;
        } else {
            if(self.show && self.detectScroll()) self.scrollerImg.style.opacity = 1;
        }
    },
    detectScroll: function () {
        if (typeof window.innerHeight === 'number') {
            this.windowHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
            this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
            this.docHeight = document.body.scrollHeight;
            return (this.windowHeight < this.docHeight && this.windowWidth > this.startMR);
        } else {
            this.scrollerImg.style.opacity = 0;
            return false;
        }
    },
    onResize: function (self, e) {
        clearTimeout(self.timeOut);
        self.scrollerImg.style.opacity = 0;
        self.timeOut = setTimeout(function() {
            self.detectScroll() ? [self.show = true, self.scrolling(self, null)] : [ self.show = false, self.scrollerImg.style.opacity = 0];
        }, 500)
    },
    init: function () {
        window.addEventListener("scroll", this.scrolling.bind(undefined, this));
        window.addEventListener('resize', this.onResize.bind(undefined, this));
        this.onResize(this);
    }
}
